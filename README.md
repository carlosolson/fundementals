# Summary
_This is a project for building a fundemental understanding of the basics of OOP(Object Oriented Programming), TDD(Test Driven Development), Git, Stress Management while navigating through the SDLC(Software Development Life Cycle) for the first time, and self documenting code in python. More to be added soon..._

> "To try and fail is at least to learn; 
> to fail to try is to suffer the inestimable loss of what might have been."
_[Algorithms to Live By](https://algorithmstoliveby.com/)_

> "Let your hopes, not your hurts, shape your future"

> "**Nothing is impossible**,<br>
>  The word itself says: <br>
>  I'm Possible"

> Success is neither magical nor mysterious. 
> Success is the natural consequence of consistently 
> applying the basic 
> fundamentals.
> _Jim Rohn_


## <u>Table Of Contents</u>
0. [Getting Started](#getting-started)
1. Programming Vocab and Usages
    0. [Solving Problems Article](https://medium.com/fintechexplained/everything-about-python-from-beginner-to-advance-level-227d52ef32d2)
    1. Data Types
    2. [Scope What is it?](https://medium.com/swlh/mastering-python-namespaces-and-scopes-7eba67aa3094) 
    3. Loops(For/While) [visualization](https://medium.datadriveninvestor.com/how-to-understand-for-and-while-loop-visually-c11052479df5) [practice](https://www.learnpython.org/en/Loops)
1. [Creating Virtual Environment](#creation)
1. [Requirements.txt](#requirements)
1. [Activation](#activation)
    1. [Windows Activation](#activation)
    1. [Mac Activation](#activation)
1. [Git](#git)
    1. [Stagging Code or Adding Code](#stagging)
    1. [Creating a new branch](#branching)
    1. [Commiting Code](#commit)
    1. [Push Changes](#push)
    1. [Resolving Conflicts](#resolve)
1. [References To Documentation](#documentation)
1. ~~[TBD](#tbd)~~


## Getting Started <a name="getting-started"></a>
- [ ] Open this project in VSCode
- [ ] Open the terminal in the **_working directory_** ```Control + Shift + ` ```
- [ ] Create a Virtual Environment [click me](#creation)
- [ ] Activate the Virtual Environment [click me](#activation)
- [ ] Run your code `python {filename.py}`
- [ ] Makes some changes and save it, test it, ect.
- [ ] Decide What you need to do in regards to [Git](#git)


## Requirements.txt <a name="requirements"></a>
**Creation**
- `pip freeze > requirements.txt`

**Installing**
- `pip install -r requirements.txt`

**Updating and adding new Packages**
- `pip install -U {package name}`


## Creating Virtual Environment <a name="creation"></a>
```python -m venv .venv```
_[reference](https://docs.python.org/3/library/venv.html)_

## <u>Activation <a name="activation"></a></u>
**Windows:**``` .\.venv\{Scripts || bin}\Activate.ps1```

**Mac:** ``` source ./.venv/{Scripts || bin}/activate```

**Deactivate:**```deactivate```

## <u>Git <a name="git"></a></u>
> _Fear not git is very forgiving! Sometimes when we're working we become unsure of **all** the changes that we've already made. Here's how to check your progress. Git is very helpful in that it will tell you what you've done to each file before you add it to staging._

![Cute Git Kitty](https://octodex.github.com/images/nyantocat.gif)

**Status and things to give you confidence <a name="status"></a>**
- `git status`
- To view commit's you've made: `git log`
- To view a certain number of changes: `git log -n {number of commits}`

**Stagging Code or Adding Code <a name="stagging"></a>**
- Add all changes: `git add .`
- Add specific File's Changes: `git add {file-name}`

**Branching <a name="branching"></a>**
- Making a new one:`git checkout -b {branch-name}`
- Switching Branches: `git checkout {branch-name}`
- List Available Branches: `git branch`
_Want to learn more here's a fun little [game](https://learngitbranching.js.org/?locale=en_US)?_

**Committing Code <a name="commit"></a>**
- `git commit -m "Some human readable code changes"`

**Push Changes <a name="push"></a>**
- Normally this is enough:` git push`
- New Branch: `git push -u origin HEAD`
    - Whats **_Origin_**: _In Git, "origin" is a shorthand name for the remote repository that a project was originally cloned from._
    - Whats **_-u_**: It is shorthand for upstream meaning you are pushing a branch that doesn't exist yet in your origin.
    - What about **_HEAD_**: A reference to what branch or commit you are currently point to.

**Pull Changes <a name="pull"></a>**
- Normally this is enough`git pull`

**Resolve Conflicts <a name="resolve"></a>**
- [ ] Check the changes that need to be made: `git status`
- [ ] Open said file in VSC(Visual Studio Code) and decide who's changes to accept. _[lost?](https://medium.com/version-control-system/git-merge-conflicts-4a18073dcc96)_
- [ ] [Stage](#stagging) or add the changes: `git add .`
- [ ] Commit the changes: `git commit -m "Merged Some Changes"`
- [ ] Push changes: `git push`
- [ ] Pat yourself on the back you made it!

## References To Documentation <a name="documentation"></a>
- [Markdown Syntax (what is a README.md File?)](https://www.markdownguide.org/basic-syntax/)
- [Python](https://docs.python.org/3/)
- [Pip](https://pypi.org/project/pip/)
    - [Pip pypa(Easier to Read)](https://pip.pypa.io/en/stable/getting-started/)
- [Github](https://docs.github.com/en)
    - [Git Branching Game](https://learngitbranching.js.org/?locale=en_US)
- [Pseudocode](https://medium.com/@ngunyimacharia/how-to-write-pseudocode-a-beginners-guide-29956242698)

## TBD... <a name="tbd"></a>

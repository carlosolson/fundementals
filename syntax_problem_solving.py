############################################################################################################
############################################### Ignore #####################################################
############################################################################################################
DEBUG = True
WELCOME = False
'''Convetional variables commonly used called a CONSTANTs used to turn functionality ON or OFF'''

def describe(parameter, message = ""):
	'''Will print out a message if debugging is turned on'''
	if not message:
		message = f"I'm a {type(parameter)} and look like: {parameter}"
	print(message if DEBUG else "")

def welcome_greeting():
	'''_Summary:_ This file will be used to Demonstrate and maintain all of your learned syntax and problem solving teqniques learned to date! Hazaa!'''
	print("Summary: This file will be used to Demonstrate and maintain all of your learned syntax and problem solving teqniques learned to date! Hazaa!\n\n\n")
welcome_greeting() if DEBUG and WELCOME else None
############################################################################################################
############################################################################################################
############################################################################################################





###############################################	Data Types	###############################################
boolean = False # False <class bool>
boolean = True 	# True
describe(boolean)

string = "" 	# False <class str>
string = '' 	# False
string = 'string' 	# Hi | True
formattedString = f"Hey, I'm a {string} {type(string)} lol"
describe(string)

integer = 0 	# 0 | False <class int>
integer = 10 	# 10 | True
describe(integer)

alist = [] 		# False <class list>
alist = [1, 2] 	# 1, 2 | True
describe(alist)

atuple = () 	# False <class tuple> Immutible **CAN'T BE CHANGED**
atuple = (1,2) 	# 1, 2 | True
describe(atuple)

########################### Conditionals, Opperators, Loops, and Functions Oh my!! ######################################

###### Conditionals and Opperators ########
"""So before we get into conditional its important to understand why...\ 
Sometimes we encouter a situation where we have to make a descition.\
For example you\'re at a crossroads driving a ship and want to figure out if you should\
turn left or right based on what based on the direction of the wind..."""
## Like that trick? using the (\) you can go to the next line like yesterdays jam

# Conditionals are ways to make descitions based on true or false values or opperators.
if True:
	print("I will always execute")
elif True:
	print("I will only execute if the first conditional is False")
else:
	print("I will only execute if the first and second conditional are False")

if True & False:
	if True and False:
		print("I will never execute since both aren't true")
elif True | False:
	if True or False:
		print("I will allways execute since one of them are True \
				and the first conditional doesn't execute")
elif False != True:
	if not False:
		print("I would always execute if the first two conditionals\
				were False because I was inverted and because\
				False is actually not True!")


## Different Values that resolve to False
print("These are different values that will be False")
bool(0)
bool(False)
bool(None)
bool("") # Empty String
bool(f"")# Empty Formatted String
bool([]) # Empty List
bool(()) # Empty Tuple
bool({}) # Empty Dictionary

## Different Values that resolve to True
print("These are different values that will be True")
bool(-1)
bool(10)
bool(1.2)
bool(True)
bool(" ")
bool(f" ")
bool([1,2])
bool([0])
bool([None])
bool((1,2))
bool((0))
bool({"Test": "This is a Test"})
bool({0: 0})

#### Loops Make the world go round
"""Why do loops make the world go round well because how else do\
you eat an elephant. You got it one byte at a time.... hahaha..."""

##### Below is an elephant String Lets eat it!!
elephant = ["e","l","e","p","h","a","n","t"]

while len(elephant) > 0: #While the size or length of the list 
						 #has things it in lets keep
	# Uncomment Me
	# print(f"Nom, nom, nom lets take a bite from {elephant}")
	# Takes a bite from frist item in the list
	del elephant[0]

#### How to make a list of people with random scores?
from random import randint
number_of_people = 40
players_in_guild = []

for player_number in range(number_of_people):
	
	a_random_score = randint(500, 3000) #Between 500 and 3000
	player = {
		"number": player_number,
		"score": a_random_score 
	}

	players_in_guild.append(player)
# uncomment Me
# print(players_in_guild)

##### How about making sure everyone in your team got high enough scores
##### for the day on seekers notes(puzzle game)?
required_score_to_be_in_the_guild = 750

for player in players_in_guild:

	player_number = player["number"]
	player_score = player["score"]
	high_enough = player_score >= required_score_to_be_in_the_guild

	if not high_enough:
		# Uncomment Me
		# print(f"Player: {player_number} doesn't have high engough of a score :(\
		# \ntheir score was {player_score}")




